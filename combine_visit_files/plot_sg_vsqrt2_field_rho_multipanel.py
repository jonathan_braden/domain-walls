import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.image as mpimg
import numpy as np
from mpl_toolkits.axes_grid1 import AxesGrid, ImageGrid
import matplotlib.gridspec as gridspec

#Start by creating the appropriately sized subplot layout

def plot_spectrum_slice():
    return

def plot_energy_contours(imfile):
    img = mpimg.imread(imfile)
    return

def plot_field_pcolor(imfile):
    img = mpimg.imread(imfile)
    plt.imshow(img)
    plt.text(0.2,0.05,r'$m_{eff}t=$',transform=plt.gca().transAxes)
    plt.axis('off')
    plt.gca().set_yticks([])
    plt.gca().set_xticks([])
    return

ind=[0000,0001,0002,0003]
times=[0,140.4,159.6,200]
basefield='sg_breather_vsqrt2_field_'
baserho='sg_vsqrt2_rhocontour_'
field_files=[]
rho_files=[]
for i in ind:
    print i
    field_files.append(basefield+'{:04}'.format(i)+'.png')
    rho_files.append(baserho+'{:04}'.format(i)+'.png')

print field_files
print rho_files

#fig = plt.figure(1,(5.6,2.8))
fig=plt.figure()
gr1 = AxesGrid(fig, 211, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)
gr2 = ImageGrid(fig, 212, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)
#gr3 = AxesGrid(fig, 313, nrows_ncols = (2,4), axes_pad=0.05, aspect=False, direction='row')

#plt.gca().cax.colorbar(img)

x=np.linspace(0.,1.,51)
y=x**2

mycm = mpl.colors.ListedColormap(['r','g','b'])

for i in range(len(field_files)):
    ax=gr1[i]
    imcur=mpimg.imread(field_files[i])
    imfld=ax.imshow(imcur,cmap=plt.get_cmap('RdBu'),vmin=-0.5*np.pi,vmax=0.5*np.pi,aspect=1)
    ax.set_yticks([])
    ax.set_xticks([])
    ax.set_axis_off()
    ax.set_title(r'$m_{SG}t='+'{:.1f}$'.format(times[i]))

    ax=gr2[i]
    imcur=mpimg.imread(rho_files[i])
    imrho=ax.imshow(imcur,cmap=mycm,vmin=0,vmax=3)
    plt.axis('off')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_axis_off()

cax=gr1.cbar_axes[0]
cb=cax.colorbar(imfld)
cax.set_ylabel(r'$\phi/\phi_0$',labelpad=-0.75)
cax.set_yticks([-0.5*np.pi,0,0.5*np.pi])
cax.set_yticklabels([r'$-\frac{\pi}{2}$',r'$0$',r'$\frac{\pi}{2}$'])
cb.solids.set_rasterized(True)

cax=gr2.cbar_axes[0]
cax.colorbar(imrho)
cax.set_ylabel(r'$\rho/\Lambda$')
cax.set_yticks([0.5,1.5,2.5])
cax.set_yticklabels([r'$\frac{1}{2}$',r'$\frac{3}{2}$',r'$\frac{5}{2}$'])
cax.tick_params(width=0)
#mpl.colorbar.ColorbarBase(cax,mycm)

plt.savefig('sg_breather_vsqrt2_field_rho_multipanel.pdf')
plt.show()
