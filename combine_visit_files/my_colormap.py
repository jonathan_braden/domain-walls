import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

def make_colormap(seq):
    """Takes as input a series of RGB colors and control points in the interval (0,1).  Returns a linearly interpolated colormap connecting these control points
    """
    seq = [(None,)*3, 0.0] + list(seq) + [1.0, (None,)*3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1,g1,b1 = seq[i-1]
            r2,g2,b2 = seq[i+1]
            cdict['red'].append([item,r1,r2])
            cdict['green'].append([item,g1,g2])
            cdict['blue'].append([item,b1,b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)

def make_sgzag(nperiod):
    space=1./nperiod
    cseq=[]
    for i in range(nperiod):
        left=i*space
        cseq.append( (0.,0.,1.) )
        cseq.append( (1.,1.,1.) )
        cseq.append( left+0.25*space )
        cseq.append( (1.,1.,1.) )
        cseq.append( (1.,0.,0.) )
        cseq.append( left+0.5*space )
        cseq.append( (1.,0.,0.) )
        cseq.append( (1.,1.,1.) )
        cseq.append( left+0.75*space )
        cseq.append( (1.,1.,1.) )
        cseq.append( (0.,0.,1.) )
        cseq.append( left+space )
    cseq.append((0.,0.,1.))
    print cseq
    return make_colormap(cseq)

def make_twowell():
    cseq=[]
    cseq=[ (1.,1.,0.),(1.,0.,0.),1./6.,
           (1.,0.,0.),(1.,1.,1.),0.5,
           (1.,1.,1.),(0.,0.,1.),5./6.,
           (0.,0.,1.),(0.,1.,1.),11./12.,
           (0.,1.,1.)
         ]
    print cseq
    return make_colormap(cseq)

def make_twowell_r():
    cseq=[ (0.,1.,1.),(0.,0.,1.),1./6.,
           (0.,0.,1.),(1.,1.,1.),0.5,
           (1.,1.,1.),(1.,0.,0.),5./6.,
           (1.,0.,0.),(1.,1.,0.),11./12.,
           (1.,1.,0.)
         ]
    print cseq
    return make_colormap(cseq)
