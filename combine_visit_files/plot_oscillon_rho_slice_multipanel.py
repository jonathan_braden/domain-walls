import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.image as mpimg
import numpy as np
from mpl_toolkits.axes_grid1 import AxesGrid, ImageGrid
import matplotlib.gridspec as gridspec

ind=[0000,0001,0002,0003]
times=[400.5,401.5,402,402.5]
baserho='oscillon_rhoslice_'
rho_files=[]
for i in ind:
    rho_files.append(baserho+'{:04}'.format(i)+'.png')

print rho_files

fig = plt.figure(1,(5.6,1.74))
gr1 = AxesGrid(fig, 111, nrows_ncols = (1,4), axes_pad=0.02, aspect=True, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)
#gr2 = ImageGrid(fig, 212, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)

mycm = mpl.colors.ListedColormap(['r','g','b'])

for i in range(len(rho_files)):
    ax=gr1[i]
    imcur=mpimg.imread(rho_files[i])
    imfld=ax.imshow(imcur,cmap=plt.get_cmap('OrRd'),vmin=0.,vmax=1.5,aspect=1)
    ax.set_yticks([])
    ax.set_xticks([])
    ax.set_axis_off()
    ax.set_title(r'$mt={:.1f}$'.format(times[i]))

cax=gr1.cbar_axes[0]
cb=cax.colorbar(imfld)
cax.set_ylabel(r'$\rho/\lambda\phi_0^4$',labelpad=-0.75)
cax.set_yticks([0,0.5,1,1.5])
cax.set_yticklabels([r'$0$',r'$\frac{1}{2}$',r'$1$',r'$\frac{3}{2}$'])
cb.solids.set_rasterized(True)

plt.savefig('oscillon_rhoslice_multipanel.pdf')
plt.show()
