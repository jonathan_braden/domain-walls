import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.image as mpimg
import numpy as np
from mpl_toolkits.axes_grid1 import AxesGrid, ImageGrid
import matplotlib.gridspec as gridspec
from my_colormap import *

#Start by creating the appropriately sized subplot layout

def plot_spectrum_slice():
    return

def plot_energy_contours(imfile):
    img = mpimg.imread(imfile)
    return

def plot_field_pcolor(imfile):
    img = mpimg.imread(imfile)
    plt.imshow(img)
    plt.text(0.2,0.05,r'$m_{eff}t=$',transform=plt.gca().transAxes)
    plt.axis('off')
    plt.gca().set_yticks([])
    plt.gca().set_xticks([])
    return

ind=[0000,0001,0002,0003]
times=[136,148.8,170,400]
basefield='v0.05_field_'
baserho='v0.05_rhocontour_'
field_files=[]
rho_files=[]
for i in ind:
    print i
    field_files.append(basefield+'{:04}'.format(i)+'.png')
    rho_files.append(baserho+'{:04}'.format(i)+'.png')

print field_files
print rho_files

#fig = plt.figure(1,(5.6,2.8))
fig=plt.figure()
gr1 = AxesGrid(fig, 211, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)
gr2 = ImageGrid(fig, 212, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)
#gr3 = AxesGrid(fig, 313, nrows_ncols = (2,4), axes_pad=0.05, aspect=False, direction='row')

#plt.gca().cax.colorbar(img)

x=np.linspace(0.,1.,51)
y=x**2

mycm = mpl.colors.ListedColormap(['r','g','b','m'])
mycm_fld = make_twowell_r()

for i in range(len(field_files)):
    ax=gr1[i]
    imcur=mpimg.imread(field_files[i])
    imfld=ax.imshow(imcur,cmap=mycm_fld,vmin=-1.5,vmax=1.5,aspect=1)
    ax.set_yticks([])
    ax.set_xticks([])
    ax.set_axis_off()
    ax.set_title(r'$mt={:.1f}$'.format(times[i]))

    ax=gr2[i]
    imcur=mpimg.imread(rho_files[i])
    imrho=ax.imshow(imcur,cmap=mycm,vmin=0,vmax=4)
    plt.axis('off')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_axis_off()

cax=gr1.cbar_axes[0]
cb=cax.colorbar(imfld)
cax.set_ylabel(r'$\phi/\phi_0$',labelpad=-0.75)
cax.set_yticks([-1.5,-1,-0.5,0,0.5,1,1.5])
cax.set_yticklabels([r'$-\frac{3}{2}$',r'$-1$',r'$-\frac{1}{2}$',r'$0$',r'$\frac{1}{2}$',r'$1$',r'$\frac{3}{2}$'])
cb.solids.set_rasterized(True)
cax.tick_params(width=0)

cax=gr2.cbar_axes[0]
cax.colorbar(imrho)
cax.set_ylabel(r'$\rho/\lambda\phi_0^4$',labelpad=0.5)
cax.set_yticks([0.5,1.5,2.5,3.5])
cax.set_yticklabels([r'$0.2$',r'$0.3$',r'$0.5$',r'$1$'])
cax.tick_params(width=0)
#mpl.colorbar.ColorbarBase(cax,mycm)

plt.savefig('v0.05_field_rho_multipanel.pdf')
plt.show()
