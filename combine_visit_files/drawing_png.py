import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('sg_breather_vsqrt2_field_0000.png')

plt.subplot(141)
plt.imshow(img)
plt.axis('off')
plt.subplot(142)
plt.imshow(img)
plt.axis('off')

plt.subplot(143)
plt.imshow(img)
plt.axis('off')

plt.subplot(144)
imgshow = plt.imshow(img,cmap=plt.get_cmap('RdBu'),vmin=-1.5,vmax=1.5)
#cb=plt.colorbar(ticks=[-1,0,1],pad=0.,shrink=0.8,aspect=20)
#cb.solids.set_rasterized(True)
#cb.set_label(r'$\phi/\phi_0$')

plt.text(0.2,0.05,r'$m_{eff}t=$',transform=plt.gca().transAxes)

plt.gca().set_yticks([])
plt.gca().set_xticks([])

plt.subplots_adjust(left=0.1,right=1,bottom=0.,top=1,hspace=0.,wspace=0.)
plt.savefig('test.pdf',dpi=250,rasterized=True)
