import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.image as mpimg
import numpy as np
from mpl_toolkits.axes_grid1 import AxesGrid, ImageGrid
import matplotlib.gridspec as gridspec
from my_colormap import *

#Start by creating the appropriately sized subplot layout

def plot_spectrum_slice():
    return

def plot_energy_contours(imfile):
    img = mpimg.imread(imfile)
    return

def plot_field_pcolor(imfile):
    img = mpimg.imread(imfile)
    plt.imshow(img)
    plt.text(0.2,0.05,r'$m_{eff}t=$',transform=plt.gca().transAxes)
    plt.axis('off')
    plt.gca().set_yticks([])
    plt.gca().set_xticks([])
    return

ind=[0000,0001,0002,0003]
times=[0,166,192,280]
basefield='sg_cascade_field_'
baserho='sg_cascade_rhocontour_'
field_files=[]
rho_files=[]
for i in ind:
    print i
    field_files.append(basefield+'{:04}'.format(i)+'.png')
    rho_files.append(baserho+'{:04}'.format(i)+'.png')

print field_files
print rho_files

fig=plt.figure()
gr1 = AxesGrid(fig, 211, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)
gr2 = ImageGrid(fig, 212, nrows_ncols = (1,4), axes_pad=0.02, aspect=False, direction='row',cbar_mode='single',cbar_location='left',cbar_size='5%',cbar_pad=0.02,share_all=True)

mycm = mpl.colors.ListedColormap(['r','g','b'])
mycm_fld = make_sgzag(4)

for i in range(len(field_files)):
    ax=gr1[i]
    imcur=mpimg.imread(field_files[i])
    imfld=ax.imshow(imcur,cmap=mycm_fld,vmin=-12.*np.pi,vmax=4.*np.pi,aspect=1)
    ax.set_yticks([])
    ax.set_xticks([])
    ax.set_axis_off()
    ax.set_title(r'$m_{SG}t='+'{:.1f}$'.format(times[i]))

    ax=gr2[i]
    imcur=mpimg.imread(rho_files[i])
    imrho=ax.imshow(imcur,cmap=mycm,vmin=0,vmax=3)
    plt.axis('off')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_axis_off()

cax=gr1.cbar_axes[0]
cb=cax.colorbar(imfld)
ytk = np.linspace(-12.*np.pi,4.*np.pi,5)
print ytk
cax.set_ylabel(r'$\phi/\phi_0$',labelpad=-0.5)
cax.set_yticks(ytk)
cax.set_yticklabels([r'$-8\pi$',r'$-4\pi$',r'$0\pi$',r'$4\pi$',r'$8\pi$'])
cb.solids.set_rasterized(True)
cax.tick_params(width=0)

cax=gr2.cbar_axes[0]
cax.colorbar(imrho)
cax.set_ylabel(r'$\rho/\Lambda$')
cax.set_yticks([0.5,1.5,2.5])
cax.set_yticklabels([r'$3$',r'$\frac{7}{2}$',r'$8$'])
cax.tick_params(width=0)
#mpl.colorbar.ColorbarBase(cax,mycm)

plt.savefig('sg_cascade_field_rho_multipanel.pdf')
plt.show()
