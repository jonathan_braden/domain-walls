#!/bin/bash

sed 's/FIG_SIZE/figure.figsize : 5.66, 3.0/g' matplotlibrc-base > matplotlibrc
echo "figure.subplot.left : 0.08" >> matplotlibrc
echo "figure.subplot.right : 0.98" >> matplotlibrc
echo "figure.subplot.bottom : 0.02" >> matplotlibrc
echo "figure.subplot.top    : 0.95" >> matplotlibrc
python plot_dw_del0.1_field_rho_multipanel.py
python plot_dw_v0.05_field_rho_multipanel.py
python plot_sg_v1_field_rho_multipanel.py
python plot_sg_vsqrt2_field_rho_multipanel.py

sed 's/FIG_SIZE/figure.figsize : 5.66, 1.5/g' matplotlibrc-base > matplotlibrc
echo "figure.subplot.left : 0.08" >> matplotlibrc
echo "figure.subplot.right : 0.98" >> matplotlibrc
echo "figure.subplot.bottom : 0.02" >> matplotlibrc
echo "figure.subplot.top    : 0.95" >> matplotlibrc
python plot_internal_field_slice_multipanel.py
python plot_oscillon_rho_slice_multipanel.py