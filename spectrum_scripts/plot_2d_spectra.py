import struct
import numpy as np

def main():
    bov_files = get_files()
    for fcur in bov_files:
        d = parse_bov_file(fcur)
    return

def get_files():
    return ['rhospec-00000.bov', 'rhospec-00100.bov']

def parse_bov_file(fname):
    """Take the given BOV file and parse into a dictionary containing information relevant to reading the raw data file"""
    info = file(fname).read().rstrip('\n').split('\n')
    pairs = [ item.split(':',1) for item in info ]
    for i in range(len(pairs)):
        pairs[i][0] = pairs[i][0].rstrip(' ').lstrip(' ')
        pairs[i][1] = pairs[i][1].rstrip(' ').lstrip(' ').split()
    bov_dict = dict( (k,v) for (k,v) in pairs )
    return bov_dict

def read_tslice(bov_dict):
    raw_file = bov_dict['DATA_FILE'][0]
    ls = bov_dict['DATA_SIZE']
    ls = [ int(sz) for sz in ls ]
    ndat = np.prod(ls)

    if bov_dict['DATA_FORMAT'][0] == 'DOUBLE':
        datsize = 'd'
    elif bov_dict['DATA_FORMAT'][0] == 'SINGLE':
        datsize = 'f'
    else:
        print 'error, undefined data precision '+bov_dict['DATA_FORMAT'][0]

    data = file(raw_file).read()
    data = struct.unpack(str(ndat)+datsize,data)
    data = np.reshape(data,ls[::-1])  # reverse ordering
    return data

def get_dx(bov_dict):
    ndim = len(bov_dict['BRICK_SIZE'])
    dx = np.zeros(ndim)
    for i in range(ndim):
        dx[i] = float(bov_dict['BRICK_SIZE'][i]) / float(bov_dict['DATA_SIZE'][i])
    return dx

import matplotlib.pyplot as plt
def plot_tslice(data, mydict):
    plt.contourf(data[0])
    plt.text( 0., 0., '{:.2f}'.format( float(mydict['TIME'][0]) ) )
    return

def make_grids():
    return

def compute_lperp():
    return

if __name__ == "__main__":
    main()
