"""Reads in binary data stored in .raw files output from my lattice simulations

This is replacing the functionality of the FortranFile class which doesn't work with my output files.
The issue seems to be the lack of a header integer in my binaries, which throws off the entire reading process.

Possible issues:
-Have not tested this approach on a multi-record file (only single record files)
"""

__docformat__ = "restructuredtext en"

import struct
import numpy as np

class BinaryFile(file):
    def __init__(self, raw_file, endian='@', header='i', *args, **kwargs):
        file.__init__(self, raw_file, *args, **kwargs)

class BovFile(file):
    def __init__(self, bov_file, endian='@', header='i', *args, **kwargs):
        file.__init__(self, bov_file, *args, **kwargs)

