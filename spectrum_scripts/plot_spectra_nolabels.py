#module load python/2.7.6 works on CITA computers
import plot_2d_spectra as sp
import numpy as np
import matplotlib.pyplot as plt

def extract_data():
    mean_field=[]
    max_field=[]
    max_loc = []
    mid_field = []
    times = []

    tvals = range(1)
    for i in tvals:
        fname = basedir+'rhospec-{:05}.bov'.format(i)
        mydict = sp.parse_bov_file(fname)
        patch_file_loc(mydict, basedir)
        data = sp.read_tslice(mydict)
        xlen = len(data[0])
        xmid = xlen/2 - 1
        klen = len(data[0][0])
#        times.append(mydict[][0])

        mean_field.append(np.sqrt(data[0,:,0]))
        maxind=get_max_index(data[0,:,0])
        max_loc.append(maxind)
        max_field.append(data[0][maxind])
        mid_field.append(data[0][xmid])

    return mean_field, max_field, max_loc, mid_field

def get_max_index(vals):
    return np.argmax(vals)

def patch_file_loc(mydict, bdir):
    mydict['DATA_FILE'][0] = bdir+mydict['DATA_FILE'][0]
    return

def read_data(tcur, bdir):
    fname = bdir+'rhospec-{:05}.bov'.format(tcur)
    mydict = sp.parse_bov_file(fname)
    patch_file_loc(mydict, bdir)
    data = sp.read_tslice(mydict)
    return mydict, data

import matplotlib.colors as mplc
import matplotlib.colorbar as mplcb
from matplotlib.ticker import MaxNLocator, LogLocator

def plot_tslice(tcur, bdir, plot_max=False, vmn=1.e-10, vmx=10., plot_label=False):
    mydict, data = read_data(tcur, bdir)

    delx = sp.get_dx(mydict); dk=delx[0]; dx=delx[1]
    lperp = 2.*np.pi / dk
    xlen = len(data[0])
    klen = len(data[0][0])

    print lperp

    kvals = np.linspace(0.,klen*dk,klen)
    xvals = np.linspace(0.,xlen*dx,xlen)
    xvals = xvals - xvals[xlen/2-1]
    xmid = xlen/2-1
    knyq = int(klen/2.**0.5)
    xmax = np.argmax(data[0,:xmid,0])

    if (plot_max):
        xplot = xmax
    else:
        xplot = xmid

    myline = np.ones(klen)*xvals[xplot]

    fig, axes = plt.subplots(2,sharex=True)
    levels = np.linspace(np.log(vmn),np.log(vmx),21)
    levels = np.exp(levels)
#    im = axes[0].contourf(kvals[:knyq],xvals,data[0,:,:knyq]*kvals[:knyq]**2*lperp**2, levels, norm=mplc.LogNorm(), vmin=vmn, vmax=vmx,extend='both')
    im = axes[0].pcolormesh(kvals[:knyq],xvals,data[0,:,:knyq]*kvals[:knyq]**2*lperp**2, norm=mplc.LogNorm(), vmin=vmn, vmax=vmx, cmap=plt.cm.Spectral,rasterized=True)
#    im = axes[0].contourf(kvals[:knyq],xvals,data[0,:,:knyq]*kvals[:knyq]**2*lperp**2, levels, extend='both')
    if plot_label:
        axes[0].set_ylabel(r'$mx_{\parallel}$')
    else:
        axes[0].yaxis.set_ticklabels([])
    axes[0].set_ylim(xvals[0],xvals[xlen-1])
    axes[0].plot(kvals[:knyq],myline[:knyq])
    axes[0].text(0.95,0.05, r'$mt={:.2f}$'.format(float(mydict['TIME'][0])), transform=axes[0].transAxes, verticalalignment='bottom', horizontalalignment='right',backgroundcolor='w')
    axes[0].yaxis.set_major_locator( MaxNLocator(5))


    axes[1].plot(kvals[:knyq],kvals[:knyq]**2*data[0,xplot,:knyq]*lperp**2)

    axes[1].set_yscale('log')
    axes[1].set_xlabel(r'$k_\perp / m$',labelpad=0.1)
    if plot_label:
        axes[1].set_ylabel(r'$\mathcal{P}_\rho^{2d}$',labelpad=-0.25)
    else:
        axes[1].yaxis.set_ticklabels([])
    axes[1].set_ylim(1.e-11,1.e2)
    axes[1].yaxis.set_major_locator( LogLocator(numticks=5) )
    axes[1].yaxis.set_ticks( [1.e-9,1.e-6,1.e-3,1.] )

#    cb_ax,kw = mplcb.make_axes([ax for ax in axes.flat], location='top')
#    plt.colorbar(im, cax=cb_ax, **kw)
#    fig.tight_layout(h_pad=0.01,pad=0.1)

    cbar = fig.colorbar(im, ax=axes.ravel().tolist(),location='top',pad=0.02,fraction=0.1,aspect=20)
    cbar.set_ticks( plt.LogLocator(numticks=5) )
    cbar.set_ticks( [1.e-9,1.e-6,1.e-3,1.] )
    cbar.solids.set_rasterized(True)
    ax=cbar.ax
    ax.text(-0.3,0.5,r'$\mathcal{P}_\rho^{2d}$')

    plt.xlim(kvals[0],kvals[knyq-1])
    return fig, axes

def plot_times(tvals,basedir,fig_name,fig_type,p_max=False):
    for tcur in tvals:
        f,a = plot_tslice(tcur,basedir,plot_max=p_max)
        plt.savefig(fig_name+'{:05}.'.format(tcur)+fig_type)
        print tcur
        plt.close(f)
    return

if __name__ == '__main__':
#    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/v1/output/'
#    mean, maxspec, maxloc, midspec = extract_data()
 
#    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/new_amplitude/absorb/dx0.125/v0.05_L64x128/output/'

    outtype='pdf'

    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/v1/output/'
    tvals=[0,135,195,400]
    plot_times(tvals,basedir,'sg_breather_v1_pspec_',outtype,p_max=False)

    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/vsqrt2/output/'
    tvals=[0,702,798,1000]
    plot_times(tvals,basedir,'sg_breather_vsqrt2_pspec_',outtype,p_max=False)

    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/new_amplitude/absorb/dx0.125/v0.05_L32x128/output/'
    tvals=[340,372,425,1000]
    plot_times(tvals,basedir,'v0.05_pspec_',outtype,p_max=False)

    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/del1o30/absorb/dx0.125/v0_r8_L64x128/output_a5/'
    tvals=[0,270,340,625]
    plot_times(tvals,basedir,'del1o30_pspec_',outtype,p_max=True)

    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/1wall_wspec/absorb/dx0.25/w2_L32x128_xdotfluc/output/'
    tvals=[0,60,85,400]
    plot_times(tvals,basedir,'shapemode_pspec_',outtype,p_max=False)

    basedir='/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/cascade_wfluc/output/'
    tvals=[0,415,480]
    plot_times(tvals,basedir,'sg_cascade_pspec_',outtype,p_max=True)
    tvals=[700]
    plot_times(tvals,basedir,'sg_cascade_pspec_',outtype,p_max=False)

    basedir='/mnt/scratch-lustre/jbraden/oscillons/Copeland_norm/r3_wspectrum/dx0.125/L64/output/'
    tvals=[4005,4015,4020,4025]
    plot_times(tvals,basedir,'oscillon_pspec_',outtype,p_max=False)
