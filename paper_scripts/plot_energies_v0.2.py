#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import matplotlib.cm as cm
import matplotlib.colors as colors

basedir = '../data/v0.2_varyamp/L64x128/'
#avals=[0,0.01,0.05,0.1,0.5,2,5]
avals=[0.15,0.21,0.215,0.22,0.23,0.24,0.25,0.3]
dx=0.25
lside=128.
lparalle=64.

ekink=2.*2.**0.5/3.

rhoslab=[]
times=[]
for acur in avals:
    infile=basedir+'log_a'+str(acur)+'.out'
    a=np.genfromtxt(infile,usecols=[0,4])
    times.append(a[:,0])
    rhoslab.append(a[:,1])

num_plots=len(rhoslab)
cNorm=colors.Normalize(vmin=0,vmax=num_plots-1)
scalarMap=cm.ScalarMappable(norm=cNorm,cmap=plt.get_cmap('brg'))
for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside**2/ekink,color=curcolor,label=lab_cur)

plt.xlim(times[0][0],times[0][len(times[0])-1])
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
plt.ylabel(r'$\sigma_{2,m|x_\parallel|<5}/\sigma_{2,kink}$')
plt.xlabel(r'$m t$')
plt.ylim(-0.1,2.5)
plt.gca().set_yticks([0.,1.,2.])
plt.gca().set_xticks([0.,200.,400.,600.,800.])
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(),plt.gca() )
plt.savefig('eslab_v0.2_varyamp.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside**2/ekink,color=curcolor,label=lab_cur)

plt.xlim(20.,times[0][len(times[0])-1])
plt.ylim(1.e-2,4.)
plt.legend(loc='lower left')
plt.ylabel(r'$\sigma_{2,m|x_\parallel|<5}/\sigma_{2,kink}$')
plt.xlabel(r'$m t$')
plt.xscale('log')
plt.yscale('log')
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect( plt.gcf(),plt.gca() )
if showPreview:
    plt.show()
plt.clf()
