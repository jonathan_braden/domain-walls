#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt

# Turn these into a dictionary
basedir='../data/internal/vary_amp/'
file_dir=["a1","a0.1","a0.01","a0.001","a0"]
labels=[r'$a=1$', r'$a=0.5$',r'$a=0.1$', r'$a=0.05$', r'$a=0.01$', r'$a=0.005$', r'$a=0.001$']
avals=[1,0.1,0.01,0.001,0]
lines=['k','r','c','g','b','r-.','c-.','g-.','b-.', 'r--']

width=32.
rho_wall = 2.*2.**0.5/3./width

times=[]
rho=[]
for fdir in file_dir:
    f = basedir+fdir+"/LOG.out"
    a=np.genfromtxt(f,usecols=[0,3])
    times.append(a[:,0])
    rho.append(a[:,1])

for i in range(len(rho)):
    plt.plot(times[i],(rho[i]-rho_wall)/rho_wall, lines[i], label=r'$32\pi\mathcal{A}_b = '+'{:.1g}'.format(avals[i])+'$')

plt.ylabel(r'$(\sigma_{2}-\sigma_{2,kink})/\sigma_{2,kink}$')
plt.xlabel(r'$m t$')
plt.xlim(1.,800.)
plt.ylim(0,0.08)
plt.gca().set_yticks([0.,0.02,0.04,0.06,0.08])
plt.gca().set_xticks([0.,200.,400.,600.,800.])
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.subplots_adjust(bottom=0.15,left=0.17)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())

#plt.yscale('log')
#plt.xscale('log')
plt.savefig('eslab_1wall_varyamp.pdf')
if showPreview:
    plt.show()
plt.clf()
