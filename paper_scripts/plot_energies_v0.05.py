#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import matplotlib.cm as cm
import matplotlib.colors as colors

basedir='../data/v0.05_varyparams/'
#avals=[0,0.01,0.05,0.1,0.5,2,5]
dirs=["L128/","L64/","L64x128/","L64x256/","L32x128_bulk/","L32x128_dx0.125/","L32x128_nofluc/"]

labels=[r"$16\sqrt{2}\pi \mathcal{A}_b=1$", #, mL_\perp=128$",
#r"$32\pi \mathcal{A}_b=1, \delta v \neq 0$", #mL_\perp=128$",
r"$16\sqrt{2}\pi \mathcal{A}_b= 1,mL_\perp=64$",  #,
r"$32\pi \mathcal{A}_b = 1, mL_\perp=128$",
r"$32\pi \mathcal{A}_b = 1, mL_\perp=256$",
r"$\lambda = 10^{-13}, mL_\perp = 128$",
r"$4\sqrt{2}\pi\mathcal{A}_b = 1, mdx=2^{-3}$",
r'$\delta\phi=0$'] #mL_\perp=128, mdx=2^{-3}$"]
lside=[128.,64.,128.,256.,128.,128.*8.**0.5,128.]
toff=[8./0.05,8./0.05,8./0.05,8./0.05,0.,0.,0.] # needed since I didn't start kinks with same separation
lside=np.array(lside)
dx=0.25
lparallel=64.

ekink=2.*2.**0.5/3.

rhoslab=[]
times=[]
for d in dirs:
    infile=basedir+d+"LOG.out"
    a=np.genfromtxt(infile,usecols=[0,4])
    times.append(a[:,0])
    rhoslab.append(a[:,1])


num_plots=len(rhoslab)
cNorm=colors.Normalize(vmin=0,vmax=num_plots-1)
scalarMap=cm.ScalarMappable(norm=cNorm,cmap=plt.get_cmap('brg'))
for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
#    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i]-toff[i],dx**3*rhoslab[i]/lside[i]**2/ekink,color=curcolor,label=labels[i])

plt.xlim(times[0][0],800.)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
plt.ylabel(r'$\sigma_{2,m|x_\parallel|<5}/\sigma_{2,kink}$')
plt.xlabel(r'$m t$')
plt.ylim(-0.1,2.5)
plt.gca().set_yticks([0.,1.,2.])
plt.gca().set_xticks([0.,200.,400.,600.,800.])
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(),plt.gca() )
plt.savefig('eslab_v0.05_new.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
#    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside[i]**2/ekink,color=curcolor,label=labels[i])

plt.xlim(20.,times[0][len(times[0])-1])
plt.ylim(1.e-2,4.)
plt.legend(loc='lower left')
plt.ylabel(r'$\sigma_{2,m|z|<5}/\sigma_{2,kink}$')
plt.xlabel(r'$m t$')
plt.xscale('log')
plt.yscale('log')
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(),plt.gca() )
#plt.savefig('eslab_v0.2_varyamp_log.pdf')
if showPreview:
    plt.show()
plt.clf()
