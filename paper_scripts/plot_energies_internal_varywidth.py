#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt

basedir = '../data/internal/vary_width/'
infiles=["LOG_w2_L32x128.dat", "LOG_w2.5_L32x128.dat","LOG_w3_L32x128.dat","LOG_w3.5_L32x128.dat"]
labels=[r'$mw=2$',r'$mw=2.5$',r'$mw=3$',r'$mw=3.5$']

length=32.
rho_bg = 2.*2.**0.5/3./length
rho_bg_box = rho_bg * 5./length

rho=[]
rho_box=[]
t=[]
for f in infiles:
    a=np.genfromtxt(basedir+f,usecols=[0,3,4])
    t.append(a[:,0])
    rho.append(a[:,1]-rho_bg)
    rho_box.append(a[:,2])

for i in range(len(t)):
    plt.plot(t[i],rho[i]/rho_bg,label=labels[i])

plt.ylabel(r'$(\sigma_{2} - \sigma_{2,kink})/\sigma_{2,kink}$')
plt.xlabel(r'$mt$')
plt.xlim(0.,800)
plt.ylim(-0.05,0.55)
plt.gca().set_xticks([0.,200.,400.,600.,800.])
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('eslab_1wall_varywidth.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in range(len(t)):
    plt.plot(t[i],rho[i]/rho_bg,label=labels[i])

plt.ylabel(r'$(\sigma_{2} - \sigma_{2,kink})/\sigma_{2,kink}$')
plt.xlabel(r'$mt$')
plt.xlim(t[0][1],800.)
plt.yscale('log')
plt.xscale('log')
plt.legend(loc='lower left')
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
#plt.savefig('eslab_1wall_varywith_log.pdf')
if showPreview:
    plt.show()
plt.clf()
