#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt

basedir='../data/oscillons/cigars/'
dx=0.25
dirs=["r2.5_eps0.1/","r2.5_eps0.2/","r2.5_eps0.3/","r2.5_eps0.4/","r2.5_eps0.5/","r2.5_eps0.6/"]
dirs=["r2.5_eps0.1/","r2.5_eps0.3/","r2.5_eps0.6/","r3_eps0.4/","r3_eps0.8/"]
eps_vals=[0.1,0.3,0.6,0.4,0.8,0.8,0.7,0.8]
r_vals=[2.5,2.5,2.5,3,3,3]

times=[]
rho=[]
eball=[]
for dcur in dirs:
    fcur=basedir+dcur+'LOG.out'
    a=np.genfromtxt(fcur,usecols=[0,3,4])
    times.append(a[:,0])
    rho.append(a[:,1])
    eball.append(a[:,2])

for i in range(len(times)):
    plt.plot(times[i],eball[i]*dx**3,label=r'$\epsilon='+'{:.1f}'.format(eps_vals[i])+', mR_{max} = '+'{:.1f}'.format(r_vals[i])+'$')

plt.xlabel(r'$mt$')
plt.ylabel(r'$\lambda m^{-1} E_{(mr<12.5)}$')
plt.title('Pancake Blobs')
plt.xlim(0.,800.)
plt.ylim(-9.,65.)
plt.gca().set_xticks([0.,200.,400.,600.,800.])
plt.legend(loc='lower right',bbox_to_anchor=(0,0.,1.08,1),borderaxespad=0.1)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('esphere_pancakes_new.pdf')
if showPreview:
    plt.show()
plt.clf()

#for i in range(len(times)):
#    plt.plot(times[i],eball[i]*dx**3,label=r'$\epsilon=$')

plt.plot(times[0],eball[0]*dx**3)
plt.xlabel(r'$mt$')
plt.ylabel(r'$E_{r<12.5}\sqrt{\lambda}\phi_0^{-1}$')
plt.title('Pancake Blobs')
plt.xlim(times[0][1],800.)
plt.yscale('log')
plt.xscale('log')
if showPreview:
    plt.show()
plt.clf()
