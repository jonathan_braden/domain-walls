#!/bin/bash
PREVIEW="True" #"False"

sed 's/FIG_SIZE/figure.figsize : 3.57, 2.4/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.16' >> matplotlibrc
echo 'figure.subplot.left   : 0.18' >> matplotlibrc
echo 'figure.subplot.top    : 0.85' >> matplotlibrc
echo 'figure.subplot.right  : 0.93' >> matplotlibrc

./plot_energies_v0.05.py $PREVIEW
./plot_energies_v0.2.py $PREVIEW
./plot_bg_collision_rho.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 2.68, 1.8/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.22' >> matplotlibrc
echo 'figure.subplot.left   : 0.21' >> matplotlibrc
echo 'figure.subplot.top    : 0.91' >> matplotlibrc
echo 'figure.subplot.right  : 0.96' >> matplotlibrc

./kink_energy_plot_varyamp.py $PREVIEW
./plot_energies_internal_varywidth.py $PREVIEW

# allow extra size at top for title
sed 's/FIG_SIZE/figure.figsize : 2.68, 1.9/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.21' >> matplotlibrc
echo 'figure.subplot.left   : 0.18' >> matplotlibrc
echo 'figure.subplot.top    : 0.86' >> matplotlibrc
echo 'figure.subplot.right  : 0.93' >> matplotlibrc
./make_enplot_oscillon_pancakes.py $PREVIEW
./make_enplot_oscillon_cigars.py $PREVIEW

for f in *.pdf
do
    gs -dBATCH -dSAFER -dNOPAUSE -dQUIET -dSubsetFonts=true -sDEVICE=pdfwrite -sOutputFile=tmp.pdf $f
    mv -f tmp.pdf $f
done