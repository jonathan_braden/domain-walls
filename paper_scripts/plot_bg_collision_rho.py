#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

def makeplot(datfile,stamp,outfile,nlat):
    a=np.genfromtxt(datfile,usecols=[0,1,4])
    xvals=np.reshape(a[:,0],(-1,nlat))
    tvals=np.reshape(a[:,1],(-1,nlat))
    fvals=np.reshape(a[:,2],(-1,nlat))

    eps=0.1
    clevs=np.arange(0.,1.+eps,eps)

#    plt.contourf(xvals,tvals,fvals,clevs,cmap=plt.cm.RdYlBu,extend='both')
    plt.pcolormesh(xvals,tvals,fvals,vmin=0.,vmax=1.,cmap=plt.cm.hot_r,rasterized=True)

    cb=plt.colorbar(extend='max',orientation='vertical',ticks=[0,0.5,1],pad=0.02)
    cb.ax.set_yticklabels([r'$0$',r'$\frac{1}{2}$',r'$1$'])
    cb.solids.set_rasterized(True)
    cb.ax.set_ylabel(r'$\rho/\lambda\phi_0^4$',rotation='vertical')
# hack to set tick sizes on colorbar
#    for tk in cb.ax.get_yticklabels():
#        tk.set_fontsize(10)

    plt.xlim(0.,200.)
    plt.ylim(-16.,16.)
    plt.gca().set_yticks([-10,0,10])
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$mx_\parallel$',labelpad=0.25)
    plt.text(0.95,0.95,stamp, bbox=dict(facecolor='white',alpha=1),transform=plt.gca().transAxes,horizontalalignment='right',verticalalignment='top')
#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())
    plt.savefig(outfile)
    if showPreview:
        plt.show()
    plt.clf()

basedir='../data/v0.2_bg/'
makeplot(basedir+'field_values_spec.out',r'$u=0.2$','1d_collision_rho_v0.2.pdf',512)
