\documentclass{revtex4}

\usepackage{amsmath, amssymb}

\begin{document}

\section{Numerical Techniques}
The results presented here were obtained using high resolution parallel lattice simulations which we outline here. %on a rectangular grid of fixed (comoving) side lengths.
%Although we have developed a new code for such simulations, the design choices and numerical techniques were strongly influenced by a number of publically available codes~\cite{ref:latticecodes}, in particular Andrei Frolov's DEFROST and Zhiqi Huang's HLATTICE.
The Laplacian and field gradient squared were approximated using 2nd-order accurate and 4th order isotropic lattice stencils from~\cite{ref:lattice_stencils,defrost}, 
and were given by
\begin{align}
  \nabla^2\phi \to L[\phi_i] = \sum_{(\alpha)} c_{(\alpha)}(\phi_{i+(\alpha)}-phi_i)\\
  (\nabla\phi)^2 \to G[\phi_i] = \sum_{(\alpha)} \frac{c_{(\alpha)}}{2}(\phi_{i+(\alpha)}-\phi_i)^2 \, .
\end{align}
with coefficients $c_{(\alpha)}$ {\bf finish} 
This pair satisfies a summation by parts $\sum_i \phi_iL[\phi_i] + G[\phi_i] = 0$, ensuring that the scalar field energy is conserved to high accuracy.

For the time evolution we used the symplectic method of Frolov and Huang~\cite{andrei,zhiqi}

In this paper, we split the Hamiltonian into two pieces $\mathcal{H} = \mathcal{H}_1 + \mathcal{H}_2$ 

Specifically, in all cases the metric was assumed to be of the form $ds^2 = -dt^2 + a(t)^2d{\bf x}^2$ with $a(t)$ some given function.
The resulting discretized Hamiltonian is
\begin{equation}
  H = \sum \frac{\pi_{\phi,i}^2}{2} + \frac{G[\phi_i]}{2} + V(\phi_i) \, .
\end{equation}
This is then decomposed into the pair of Hamiltonians $\mathcal{H}_1 = \pi_\phi^2/2$ and $\mathcal{H}_2 = G[\phi_i]/2 + V(\phi_i)$.
Each of these gives a field evolution that can be explicitly solved:
\begin{align}
  \mathcal{H}_1 &: \pi_i(t+dt) \to \pi_i(t) \\
  \mathcal{H}_1 &: \phi_i(t+dt) \to \phi_i(t) + dt \pi_i(t)
\end{align}
and
\begin{align}
  \mathcal{H}_2 &: \pi_i(t+dt) = \pi_i + dt ( L[\phi_i(t)] - V'(\phi_i(t)) )\\
  \mathcal{H}_2 &: \phi_i(t+dt) = \phi_i(t) \, .
\end{align}

We now outline the implementation of absorbing boundary conditions in Minkowski space, following closely the treatment in~\cite{Salmi:2012ta, ref:boundcond}.
Consider a wave propagating in the $-x$ direction as it encounters the left-hand boundary of the lattice.
Treating this as a small fluctuation around a minima of the potential, this wave has the form
\begin{equation}
  \delta\phi \propto e^{i(\omega t + (k_xx + k_yy + k_zz))} \, .
\end{equation}
with $k_x^2 = \omega^2 - k_y^2 - k_z^2 - V''(\phi_{min})$, where $\phi_{min}$ is the field value at the local minimum of the potential.
This wave is annihilated by the (pseudodifferential) operator
\begin{equation}
  -i\left(\frac{\partial}{\partial x} - \sqrt{\omega^2 - (k_y^2+k_z^2+V''(\phi_{min}))}\right)e^{i(\omega t + (k_xx+k_yy+k_zz)} = 0 \, .
\label{eqn:annihilation_boundary}
\end{equation}
In order to obtain a differential operator suitable for numerical evolution,
we expand the square root in inverse powers of $\omega$ (noting that $\omega^2 > {\bf k}_2^2$) and replace $i\omega = \frac{\partial}{\partial t}$.
Recalling that we are expanding around a constant homogeneous background, at leading order in $\omega$,~\eqref{eqn:annihilation_boundary} becomes $\partial_x\delta\phi = \partial_t\delta\phi$.
% For completeness we also include the second order conditions, whose implementation we leave to future work

\section{Initial Conditions for Inhomogeneous Background Field Configurations}
As mentioned in the main text, the key feature of our simulations that lead to substantial deviations from previous work is the inclusion of small fluctuations around the prescribed background.
The treatment of this problem for homogeneous backgrounds is well developed in applications of numerical simulations to preheating.~\cite{preheat_ic}
We now generalize this conventional choice to the case when the background fields have spatial structure.~\cite{ref:kinkquantize,ref:garriga_guven}.

For simplicity, we will only treat the case of planar domain walls as it illustrates the main points and avoids many complications associated with more complex geometries and time-dependent backgrounds.
In our actual simulations, we tested several choices of initial fluctuations with various amounts of spatial dependence along the collision direction, fourier spectra and amplitudes.
For all cases we tested, the qualitative results outlined in this paper were completely unchanged.

Consider a domain wall at rest.
The moving domain wall is obtained by boosting the resting solution.
Decompose the field operator into a classical background and fluctuation operator
\begin{equation}
  \hat{\phi}(\vec{x},t) = \phi_{dw}(x) + \hat{\delta\phi}(\vec{x},t) \, .
\end{equation}
The field fluctuation operator is now expanded into a set of normal modes as
\begin{equation}
  \hat{\delta\phi} = \sum_i \int \frac{d^2k}{2\pi}(\hat{a_i}f_i(x)e^{i(k_yy+k_zz-\omega_{i,k_2} t)} + h.c)
\end{equation}
where the $a_i$'s are annihilation operators and the $f_i$'s are the eigenfunction solutions of
\begin{equation}
  \frac{\partial^2 f_i(x)}{\partial x^2} + (\omega_{i,k_\perp}^2 - k_\perp^2 - V''(\phi_{dw}(x)))f_i(x) = 0
  \label{eqn:fluceqn}
\end{equation}
with $k_\perp^2 = k_y^2 + k_z^2$.
The allowed eigenfrequencies now depend on the form of $V''(\phi_{dw})$, which in turn is determined by our choice of potential (both through the functional form of $V''$ and also through the solution $\phi_{bg}$.
Identifying $\omega_{eff}^2 = \omega^2 - k_\perp^2$, this is simply the time-independent Schrodinger equation for the energy eigenstates $\omega_{eff}^2$ in potential $V''(x)$.
For the double-well potential~\eqref{eqn:doublewell} and corresponding kink solution~\eqref{eqn:phikink} (with periodic b.c.'s), 
this equation has two bound state solutions of energy $\omega_{eff}^2 = 0$ and $3\lambda\phi_0^2/2$, as well as a continuum of scattering states with $\omega_{eff}^2(q) = \lambda\phi_0^2\left(q^2/2 + 2\right)$.
When we were to make the collision direction of finite size and impose either periodic boundary conditions,
the continuum states will become discrete with the allowed eigenvalues determined by a condition on the overall phase acquired between the two ends of the box.
In the context of (linear) field fluctuations, the bound state fluctuations are localized near the domain wall and thus correspond to excitations of the internal degrees of freedom of the wall.  
The exception to this is the zero energy solution (with $k_2^2=0$ and $\omega^2 = 0$) which is of course just the linearized approximation for the translation mode of the wall.
The scattering states meanwhile correspond to bulk fluctuations.
The only remaining ingredient is the normalization of the eigenmodes $f_i$, which is given by
\begin{equation}
  \int dx |f_i(x)|^2 = (2\omega_{i,k_2})^{-1} = (2\sqrt{\omega_{eff,i}^2 + k_2^2})^{-1} \, .
  \label{eqn:normalization}
\end{equation}
for the discrete bound state $f_i$'s and a similar equation with appropriate normalization for the bulk fluctuations.

We now recall a well known general observation that can be applied to any domain wall with zero-energy translation mode.
By differentiating~\eqref{eqn:kinkeqn}, we see that $\partial_x\phi_{dw}(x)$ is a solution of~\eqref{eqn:fluceqn} with $\omega_{eff}^2=0$.
Thus, we can write our expansion in eigenmodes at $t=0$ as
\begin{equation}
  \delta\phi = \frac{\phi_{dw}'}{\sqrt{\int dx\phi_{dw}'^2}} \int d^2k(\frac{e^{i\vec{k}\vec{y}}}{2\pi\sqrt{2k_2}}\hat{a_0} + h.c) +  \sum_{i, \omega_{eff,i}\neq 0} \int \frac{d^2k}{2\pi}(\hat{a_i}f_i(x)e^{i(k_yy+k_zz-\omega_{i,k_2} t)} + h.c) \, .
\end{equation}
Now let's instead suppose that we allow the location of the domain wall to become a function of $y$ and $z$, but otherwise do not deform it's shape.
We may write the resulting field configuration as
\begin{equation}
  \phi(x) = \phi_{bg}(x+\delta x(y,z)) \approx \phi_{bg}(x) + \phi_{bg}'(x)\delta x + \cdots \, .
\end{equation}
Comparing to our previous expansion, we see that the fluctuations corresponding to the lowest energy bound state may (in the linear regime) be interpreted as a 2-d random field $\delta x$ of displacements in the wall locations with mean square fourier amplitudes 
\begin{equation}
  \langle |\tilde{\delta x}_k|^2 \rangle = \frac{1}{2k_2\sigma}
\end{equation}
where in the last step we have assumed we are in the vacuum state $|0\rangle$ with $\hat{a}|0\rangle = 0$ and the surface tension of the wall is given by $\sigma = \int dx (\phi_{dw}')^2$.

In passing from the quantum to the classical description, we must replace the quantum operator $\hat{\delta\phi}$ by a classical random field.
For an initial vacuum state, this is accomplished by treating the creation operators $\hat{a}$ a gaussian random variables with unit variance,
which reproduces the correct variance for the fourier modes $\langle|\delta\phi_k|^2\rangle$.
A correct description would include all of the bound as well as bulk states for the system.
However, as a first approximation we have included only the least energetic fluctuations (corresponding to $\omega_{eff,i} = 0$) and initialized these
as a 2-d gaussian random field $\delta x(y,z)$ for the initial location of the wall as outlined above.
This was primarily for convenience as this procedure can easily be adapted to other potentials as it only requires knowledge of the background solution, not a solution to the full eigenvalue problem for the linearized fluctuations.

\end{document}
