import numpy as np
import matplotlib.pyplot as plt

#avals=[0,0.01,0.05,0.1,0.5,2,5]
avals=[0,0.1,0.5,2]
dx=0.25
lside=64.

ekink=2.*2.**0.5/3.

rhoslab=[]
times=[]
for acur in avals:
    infile='log_a'+str(acur)+'.out'
    a=np.genfromtxt(infile,usecols=[0,4])
    times.append(a[:,0])
    rhoslab.append(a[:,1])

for i in range(len(rhoslab)):
    lab_cur = r'$32\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside**2/ekink,label=lab_cur)

plt.xlim(times[0][0],times[0][len(times[0])-1])
plt.legend(loc='upper right',fontsize=20)
plt.ylabel(r'$\sigma_{m|z|<5}/\sigma_{kink}$',fontsize=30)
plt.xlabel(r'$m t$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('eslab_v0.2_varyamp.pdf')
plt.show()
