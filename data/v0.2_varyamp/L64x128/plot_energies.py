import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.colors as colors

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

#avals=[0,0.01,0.05,0.1,0.5,2,5]
avals=[0.15,0.21,0.215,0.22,0.23,0.24,0.25,0.3]
dx=0.25
lside=128.
lparalle=64.

ekink=2.*2.**0.5/3.

rhoslab=[]
times=[]
for acur in avals:
    infile='log_a'+str(acur)+'.out'
    a=np.genfromtxt(infile,usecols=[0,4])
    times.append(a[:,0])
    rhoslab.append(a[:,1])


num_plots=len(rhoslab)
cNorm=colors.Normalize(vmin=0,vmax=num_plots-1)
scalarMap=cm.ScalarMappable(norm=cNorm,cmap=plt.get_cmap('brg'))
for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside**2/ekink,color=curcolor,label=lab_cur)

plt.xlim(times[0][0],times[0][len(times[0])-1])
plt.legend(loc='upper right',fontsize=20,bbox_to_anchor=(0,0,1.1,1.1))
plt.ylabel(r'$\sigma_{2,m|z|<5}/\sigma_{2,kink}$',fontsize=30)
plt.xlabel(r'$m t$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('eslab_v0.2_varyamp.pdf')
plt.show()

for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside**2/ekink,color=curcolor,label=lab_cur)

plt.xlim(20.,times[0][len(times[0])-1])
plt.ylim(1.e-2,4.)
plt.legend(loc='lower left',fontsize=20)
plt.ylabel(r'$\sigma_{2,m|z|<5}/\sigma_{2,kink}$',fontsize=30)
plt.xlabel(r'$m t$',fontsize=30)
plt.xscale('log')
plt.yscale('log')
plt.subplots_adjust(bottom=0.15,left=0.15)
#plt.savefig('eslab_v0.2_varyamp_log.pdf')
plt.show()
