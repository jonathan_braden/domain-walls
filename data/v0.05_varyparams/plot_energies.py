import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.colors as colors

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

#avals=[0,0.01,0.05,0.1,0.5,2,5]
dirs=["L128/","L64/","L64x128/","L64x256/","L32x128_bulk/","L32x128_dx0.125/","L32x128_nofluc/"]

labels=[r"$16\sqrt{2}\pi \mathcal{A}_b=1$", #, mL_\perp=128$",
#r"$32\pi \mathcal{A}_b=1, \delta v \neq 0$", #mL_\perp=128$",
r"$16\sqrt{2}\pi \mathcal{A}_b= 1,mL_\perp=64$",  #,
r"$32\pi \mathcal{A}_b = 1, mL_\perp=128$",
r"$32\pi \mathcal{A}_b = 1, mL_\perp=256$",
r"$\lambda = 10^{-13}, mL_\perp = 128$",
r"$4\sqrt{2}\pi\mathcal{A}_b = 1, mdx=2^{-3}$",
r'$\delta\phi=0$'] #mL_\perp=128, mdx=2^{-3}$"]
lside=[128.,64.,128.,256.,128.,128.*8.**0.5,128.]
toff=[8./0.05,8./0.05,8./0.05,8./0.05,0.,0.,0.] # needed since I didn't start kinks with same separation
lside=np.array(lside)
dx=0.25
lparallel=64.

ekink=2.*2.**0.5/3.

rhoslab=[]
times=[]
for d in dirs:
    infile=d+"LOG.out"
    a=np.genfromtxt(infile,usecols=[0,4])
    times.append(a[:,0])
    rhoslab.append(a[:,1])


num_plots=len(rhoslab)
cNorm=colors.Normalize(vmin=0,vmax=num_plots-1)
scalarMap=cm.ScalarMappable(norm=cNorm,cmap=plt.get_cmap('brg'))
for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
#    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i]-toff[i],dx**3*rhoslab[i]/lside[i]**2/ekink,color=curcolor,label=labels[i])

plt.xlim(times[0][0],800.)
plt.legend(loc='upper right',fontsize=16,bbox_to_anchor=(0,0,1.15,1.15))
plt.ylabel(r'$\sigma_{2,m|x_\parallel|<5}/\sigma_{2,kink}$',fontsize=30)
plt.xlabel(r'$m t$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('eslab_v0.05_new.pdf')
plt.show()

for i in range(len(rhoslab)):
    curcolor=scalarMap.to_rgba(i)
#    lab_cur = r'$16\sqrt{2}\pi\mathcal{A}_b = '+str(avals[i])+'$'
    plt.plot(times[i],dx**3*rhoslab[i]/lside[i]**2/ekink,color=curcolor,label=labels[i])

plt.xlim(20.,times[0][len(times[0])-1])
plt.ylim(1.e-2,4.)
plt.legend(loc='lower left',fontsize=20)
plt.ylabel(r'$\sigma_{2,m|z|<5}/\sigma_{2,kink}$',fontsize=30)
plt.xlabel(r'$m t$',fontsize=30)
plt.xscale('log')
plt.yscale('log')
plt.subplots_adjust(bottom=0.15,left=0.15)
#plt.savefig('eslab_v0.2_varyamp_log.pdf')
plt.show()
