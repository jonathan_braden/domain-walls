import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

dx=0.25
dirs=["r2.5_eps0.1/","r2.5_eps0.2/","r2.5_eps0.3/","r2.5_eps0.4/","r2.5_eps0.5/","r2.5_eps0.6/"]
dirs=["r2.5_eps0.1/","r2.5_eps0.3/","r2.5_eps0.6/","r3_eps0.4/","r3_eps0.8/"]
eps_vals=[0.1,0.3,0.6,0.4,0.8,0.8,0.7,0.8]
r_vals=[2.5,2.5,2.5,3,3,3]

times=[]
rho=[]
eball=[]
for dcur in dirs:
    fcur=dcur+'LOG.out'
    a=np.genfromtxt(fcur,usecols=[0,3,4])
    times.append(a[:,0])
    rho.append(a[:,1])
    eball.append(a[:,2])

for i in range(len(times)):
    plt.plot(times[i],eball[i]*dx**3,label=r'$\epsilon='+'{:.1f}'.format(eps_vals[i])+', mR_{max} = '+'{:.1f}'.format(r_vals[i])+'$',linewidth=1.5)

plt.xlabel(r'$mt$',fontsize=30)
plt.ylabel(r'$\lambda m^{-1} E_{(mr<12.5)}$',fontsize=30)
plt.title('Pancake Blobs',fontsize=30)
plt.xlim(0.,800.)
plt.ylim(0.,65.)
plt.legend(loc='lower right',fontsize=24,bbox_to_anchor=(0,-0.04,1.03,1))
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('esphere_pancakes_new.pdf')
plt.show()

#for i in range(len(times)):
#    plt.plot(times[i],eball[i]*dx**3,label=r'$\epsilon=$')

plt.plot(times[0],eball[0]*dx**3)
plt.xlabel(r'$mt$',fontsize=30)
plt.ylabel(r'$E_{r<12.5}\sqrt{\lambda}\phi_0^{-1}$',fontsize=30)
plt.title('Pancake Blobs',fontsize=30)
plt.xlim(times[0][1],800.)
plt.yscale('log')
plt.xscale('log')
plt.show()
