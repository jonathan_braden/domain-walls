import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

infiles=["LOG_w2_L32x128.dat", "LOG_w2.5_L32x128.dat","LOG_w3_L32x128.dat","LOG_w3.5_L32x128.dat"]
labels=[r'$mw=2$',r'$mw=2.5$',r'$mw=3$',r'$mw=3.5$']

length=32.
rho_bg = 2.*2.**0.5/3./length
rho_bg_box = rho_bg * 5./length

rho=[]
rho_box=[]
t=[]
for f in infiles:
    a=np.genfromtxt(f,usecols=[0,3,4])
    t.append(a[:,0])
    rho.append(a[:,1]-rho_bg)
    rho_box.append(a[:,2])

for i in range(len(t)):
    plt.plot(t[i],rho[i]/rho_bg,label=labels[i],linewidth=1.5)

plt.ylabel(r'$(\sigma_{2} - \sigma_{2,kink})/\sigma_{2,kink}$',fontsize=30)
plt.xlabel(r'$mt$',fontsize=30)
plt.xlim(0.,800)
plt.legend(loc='upper right',fontsize=26)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('eslab_1wall_varywidth.pdf')
plt.show()

for i in range(len(t)):
    plt.plot(t[i],rho[i]/rho_bg,label=labels[i],linewidth=1.5)

plt.ylabel(r'$(E_{2d} - E_{2d}^{kink})/E_{2d}^{kink}$',fontsize=30)
plt.xlabel(r'$mt$',fontsize=30)
plt.xlim(t[0][1],800.)
plt.yscale('log')
plt.xscale('log')
plt.legend(loc='lower left',fontsize=26)
plt.subplots_adjust(bottom=0.15,left=0.17)
plt.savefig('eslab_1wall_varywith_log.pdf')
plt.show()
