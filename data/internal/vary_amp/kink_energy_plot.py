import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

# Turn these into a dictionary
file_dir=["a1","a0.1","a0.01","a0.001","a0"]
labels=[r'$a=1$', r'$a=0.5$',r'$a=0.1$', r'$a=0.05$', r'$a=0.01$', r'$a=0.005$', r'$a=0.001$']
avals=[1,0.1,0.01,0.001,0]
lines=['k','r','c','g','b','r-.','c-.','g-.','b-.', 'r--']

width=32.
rho_wall = 2.*2.**0.5/3./width

times=[]
rho=[]
for fdir in file_dir:
    f = fdir+"/LOG.out"
    a=np.genfromtxt(f,usecols=[0,3])
    times.append(a[:,0])
    rho.append(a[:,1])

for i in range(len(rho)):
    plt.plot(times[i],(rho[i]-rho_wall)/rho_wall, lines[i], label=r'$32\pi\mathcal{A}_b = '+'{:.1g}'.format(avals[i])+'$', linewidth=1.5, markersize=4.)

plt.ylabel(r'$(\sigma_{2}-\sigma_{2,kink})/\sigma_{2,kink}$',fontsize=30)
plt.xlabel(r'$m t$',fontsize=30)
plt.xlim(1.,800.)
plt.legend(loc='upper right',fontsize=22)
plt.subplots_adjust(bottom=0.15,left=0.17)

#plt.yscale('log')
#plt.xscale('log')
plt.savefig('eslab_1wall_varyamp.pdf')
plt.show()
