import numpy as np
import matplotlib.pyplot as plt

infiles=["L32x128_nofluc", "L32x128_bulk_l1e5","LOG_w2_L32x128_bul_l1e5_massless","LOG_w3.5_L32x128.dat"]
labels=[r'$\delta x=0$',r'$\lambda=$',r'$\lambda=$',r'$mL=128$',r'$mL=256$']

rho_bg = 2.*2.**0.5/3./32.
rho_bg_box = rho_bg * 5./32.

rho=[]
rho_box=[]
t=[]
for f in infiles:
    a=np.genfromtxt(f,usecols=[0,3,4])
    t.append(a[:,0])
    rho.append(a[:,1]-rho_bg)
    rho_box.append(a[:,2])

for i in range(len(t)):
    plt.plot(t[i],rho[i],label=labels[i],linewidth=1.5)

plt.ylabel(r'$\rho - \rho_{kink}$',fontsize=18)
plt.xlabel(r'$mt$',fontsize=18)
plt.xlim(0.,800)
plt.legend(loc='upper right')
