PaperPlots=True
MoviePlots=False

#run_selection='sg_v1'
#run_selection='sg_vsqrt2'
#run_selection='v0.05'
#run_selection='del0.1'
#run_selection='sg_cascade'

#v=1 SineGordon
if run_selection=='sg_v1':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/v1/output/"
    levels=(1.,2.5,5.)
    tslices=[0,135,195,400]
    base_file="sg_v1_rhocontour_"

#v=(sqrt2-1)^{-1} SineGordon
elif run_selection=='sg_vsqrt2':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/vsqrt2/output/"
    levels=(0.5,2.5,5)
    tslices=[0,702,798,1000]
    base_file="sg_vsqrt2_rhocontour_"

#v=0.05 symmetric double well
elif run_selection=='v0.05':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/new_amplitude/absorb/dx0.125/v0.05_L32x128/output/"
    levels=(0.2,0.3,0.5,1)
    tslices=(340,372,425,1000)
    base_file="v0.05_rhocontour_"

elif run_selection=='del0.1':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/del1o30/absorb/dx0.125/v0_r8_L64x128/output_a5/"
    levels=(0.2,0.3,0.5,1)
    tslices=(0,270,340,625)
    base_file="del1o30_rhocontour_"

elif run_selection=='sg_cascade':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/cascade_wfluc/output/"
    levels=(2.,3.5,8.)
    tslices=[0,415,480,700]
    base_file="sg_cascade_rhocontour_"

else:
    print "Error, invalid plot selection"

figheight=1024
figwidth=1024

varname="rhonorm"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

v=GetView3D()
v.viewNormal = (0.3,0.5,0.5)
v.imageZoom=1.15
SetView3D(v)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
SetAnnotationAttributes(a)

a.backgroundMode=1
a.gradientBackgroundStyle=0
a.gradientColor1 = (192,192,192,255)
a.gradientColor2 = (255,255,255,255)
SetAnnotationAttributes(a)

AddPlot("Contour",varname)
ca=ContourAttributes()
ca.contourNLevels=len(levels)
ca.contourMethod=ca.Value
ca.contourValue=levels
ca.SetMultiColor(0, (255,0,0,25))
ca.SetMultiColor(1, (0,255,0,50))
ca.SetMultiColor(2, (0,0,255,100))
ca.SetMultiColor(3, (255,0,255,125))
ca.SetMultiColor(4, (0,128,128,150))
SetPlotOptions(ca)

ca.legendFlag=0
SetPlotOptions(ca)
#objs=GetAnnotationObjectNames()
#l=GetAnnotationObject(objs[0])
#l.drawTitle=0
#l.drawMinMax=0
#l.orientation=l.VerticalRight
#l.numberFormat="%# -5.2g"
#l.managePosition=1
#l.position=(0.04,0.98)
#l.xScale=0.5
#l.yScale=0.7
#l.fontBold=1
#l.fontHeight=0.12
#l.fontFamily=2

#ts=CreateAnnotationObject("TimeSlider")
#ts.position=(0.2,0.05)
#ts.height=0.1
#ts.width=0.5

DrawPlots()

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=1
SetSaveWindowAttributes(sw)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        DrawPlots()
        SetPlotOptions(ca) # Evil hack to get the axes to be visible
        SaveWindow()

if MoviePlots:
    for i in range(TimeSliderGetNStates()):
        SetTimeSliderState(i)
        DrawPlots()
        SetPlotOptions(ca)
        SaveWindow()
