PaperPlots=True

rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/cascade_wfluc/output/"
levels=(2.,3.5,8.)
tslices=[0,415,480,700]

figheight=720
figwidth=720

varname="rhonorm"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

v=GetView3D()
v.viewNormal = (0.3,0.5,0.5)
SetView3D(v)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
SetAnnotationAttributes(a)

AddPlot("Contour",varname)
ca=ContourAttributes()
ca.contourNLevels=len(levels)
ca.contourMethod=ca.Value
ca.contourValue=levels
ca.SetMultiColor(0, (255,0,0,25))
ca.SetMultiColor(1, (0,255,0,50))
ca.SetMultiColor(2, (0,0,255,100))
ca.SetMultiColor(3, (255,0,255,125))
ca.SetMultiColor(4, (0,128,128,150))
SetPlotOptions(ca)
DrawPlots()

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.05,0.95)
l.xScale=0.5
l.yScale=2
l.fontBold=1
l.fontHeight=0.05

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.2,0.05)

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName="rho_contours_"
sw.resConstraint=sw.NoConstraint
sw.screenCapture=1
SetSaveWindowAttributes(sw)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        DrawPlots()
        SetPlotOptions(ca) # Evil hack to get the axes to be visible
        SaveWindow()

