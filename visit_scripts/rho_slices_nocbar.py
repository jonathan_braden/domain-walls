PaperPlots=True

rootdir="/mnt/scratch-lustre/jbraden/oscillons/Copeland_norm/r3_wspectrum/dx0.125/L64/output/"
tslices=[4005,4015,4020,4025]
length=64
mycolortable="OrRd"
minval,maxval=0,1.5
zscale=20

figheight=1024
figwidth=1024

varname="rhonorm"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.visible=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
a.axes3D.bboxLocation=(0,length,0,length,0,zscale*maxval)
a.axes3D.setBBoxLocation=1
SetAnnotationAttributes(a)

v=GetView3D()
v.viewNormal = (-0.7,0.5,0.5)
v.viewUp=(0,0,1)
v.focus=(length/2.,length/2.,0.25*zscale*maxval)
v.imageZoom=2.
v.parallelScale=50
v.farPlane=-90
v.nearPlane=90
SetView3D(v)

AddPlot("Pseudocolor",varname)
pa=PseudocolorAttributes()
pa.colorTableName=mycolortable
pa.minFlag=1
pa.maxFlag=1
pa.min=minval
pa.max=maxval
SetPlotOptions(pa)

pa.legendFlag=0
SetPlotOptions(pa)
#objs=GetAnnotationObjectNames()
#l=GetAnnotationObject(objs[0])
#l.drawTitle=0
#l.drawMinMax=1
#l.orientation=l.VerticalRight
#l.numberFormat="%# -5.2g"
#l.managePosition=1
#l.position=(0.05,0.95)
#l.xScale=0.5
#l.yScale=3
#l.fontBold=1
#l.fontHeight=0.12
#l.fontFamily=2
#l.numTicks=0

AddOperator("Box")
bo=GetOperatorOptions(0)
bo.minx=0.25*length
bo.maxx=0.75*length
bo.miny=0.25*length
bo.maxy=0.75*length
bo.minz=0.25*length
bo.maxz=0.75*length
SetOperatorOptions(bo)

AddOperator("Slice")
sa=SliceAttributes()
sa.originType=sa.Intercept
sa.originIntercept=length/2.
sa.axisType=sa.YAxis
sa.project2d=1
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta=TransformAttributes()
ta.doScale=1
ta.scaleZ=zscale
SetOperatorOptions(ta)

#ts=CreateAnnotationObject("TimeSlider")
#ts.position=(0.5,0.05)
#ts.height=0.1
#ts.width=0.5

DrawPlots()

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName="rho_slices_"
sw.resConstraint=sw.NoConstraint
sw.screenCapture=1
SetSaveWindowAttributes(sw)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        SetView3D(v)
        SaveWindow()
