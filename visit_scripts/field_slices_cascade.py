PaperPlots=True

def MakeRGBColorTable(name,ct):
    ccpl = ColorControlPointList()
    for pt in ct:
        p = ColorControlPoint()
        p.colors = (pt[0]*255, pt[1]*255, pt[2]*255, 255)
        p.position = pt[3]
        ccpl.AddControlPoints(p)
    AddColorTable(name,ccpl)

nperiod = 4
space = 1./nperiod
mypi = 3.14159
cpoint_sg = []
for i in range(nperiod):
    left=i*space
    cpoint_sg.append( (0.,0.,1.,left) )
    cpoint_sg.append( (1.,1.,1.,left+0.25*space) )
    cpoint_sg.append( (1.,0.,0.,left+0.5*space) )
    cpoint_sg.append( (1.,1.,1.,left+0.75*space) )
    cpoint_sg.append( (0.,0.,1.,left+space) )
MakeRGBColorTable("SG_zag", cpoint_sg)          

rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/cascade_wfluc/output/"
tslices=[480,700] #[0][415][480,700]
xcent=8.
num_pis=4.
xcents=[4.,6.,8.,8.]
n_pis=[-8,5,4,4]
length=64.
width=16.
mycolortable="SG_zag"
minval,maxval=-12.*mypi,4.*mypi
base_file="sg_cascade_field_"

figheight=430
figwidth=430

varname="phi1"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
a.axes3D.bboxLocation=(0,length,0,width,minval,maxval)
a.axes3D.setBBoxLocation=1
SetAnnotationAttributes(a)

v=GetView3D()
v.viewNormal = (0.7,-0.5,0.2)
v.viewUp=(0,0,1)
v.focus=(length/2.,width/2.,-25.)
v.imageZoom=0.9
v.parallelScale=40
SetView3D(v)

AddPlot("Pseudocolor",varname)
pa=PseudocolorAttributes()
pa.colorTableName=mycolortable
pa.minFlag=1
pa.maxFlag=1
pa.min=minval
pa.max=maxval
pa.smoothingLevel=2
SetPlotOptions(pa)

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.05,0.95)
l.xScale=0.5
l.yScale=3
l.fontBold=1
l.fontHeight=0.12
l.fontFamily=2

AddOperator("Slice")
sa=SliceAttributes()
sa.originType=sa.Intercept
sa.originIntercept=length/2.
sa.axisType=sa.YAxis
sa.project2d=1
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta=TransformAttributes()
ta.doScale=1
ta.scaleZ=1
SetOperatorOptions(ta)

AddPlot("Pseudocolor",varname)
pa.legendFlag=0
SetPlotOptions(pa)

AddOperator("Slice")
sa.originIntercept=xcent
sa.axisType=sa.ZAxis
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta.doScale=1
ta.doTranslate=0
ta.scaleZ=1.
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doScale=0
ta.doRotate=1
ta.rotateAmount=90
ta.rotateAxis=(1,0,0)
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=0
ta.doTranslate=1
ta.translateY=-num_pis*mypi
ta.translateZ=minval
SetOperatorOptions(ta)

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.2,0.05)
ts.height=0.1
ts.width=0.5

DrawPlots()

v=GetView3D()
v.focus=(length/2.,width/2.,-10.)
SetView3D(v)

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=0
SetSaveWindowAttributes(sw)

#for tcur in range(500):
#    SetTimeSliderState(tcur)
#    SaveWindow()

SetTimeSliderState(200)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        SetView3D(v)
        SaveWindow()
