paper_plots=True
movie_plots=False

#rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/new_amplitude/absorb/dx0.125/v0.05_L32x128/output/"
#length=32.
#lperp=128.
#box_width=12.
#xcent=length/2.
#kmax=25.
#tslices=[340,372,425,1000]
#ampmin,ampmax = -5,1
#pcolormin,pcolormax=1.e-5,0.1
#file_base="v0.05_pspec_"
#nlat=

#rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/v1/output/"
#length=64.
#lperp=128.
#xcent=length/2.
#box_width=16.
#kmax=12.
#tslices=[0]#,135,195,400]
#ampmin,ampmax = -8,2
#pcolormin,pcolormax=1.e-8,1.e-4
#pcolormin,pcolormax=1.e-3,10.
#file_base="sg_breather_v1_pspec_"
#nlat=

#rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/vsqrt2/output/"
#length=64.
#lperp=128.
#xcent=length/2.
#kmax=12.
#tslices=[0,702,798,1000]
#box_width=16.
#ampmin,ampmax = -9, 2  # Log of min/max amplitude for sliced spectrum
#pcolormin,pcolormax=1.e-7,1.e-5
#pcolormin,pcolormax=5.e-4,5.
#file_base="sg_breather_vsqrt2_pspec_"
#nlat=

#rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/1wall_wspec/absorb/dx0.25/w2_L32x128_xdotfluc/output/"
#tslices=[0,60,85,400]
#length=32
#lperp=128.
#xcent=length/2.
#box_width=16.
#kmax=15.
#ampmin,ampmax=-8.,0.
#pcolormin,pcolormax=1.e-9,1.e-3
#file_base="shapemode_pspec_"
#nlat=

rootdir="/mnt/scratch-lustre/jbraden/oscillons/Copeland_norm/r3_wspectrum/dx0.125/L64/output/"
tslices=[4005,4015,4020,4025]
length=64.
lperp=64.
xcent=length/2.
box_width=16.
kmax=12.
ampmin,ampmax=-10.,-1.
pcolormin,pcolormax=5.e-8,0.05
file_base="oscillon_pspec_"
#nlat=512

#rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/del1o30/absorb/dx0.125/v0_r8_L64x128/output_a5/"
#length=64.
#lperp=128.
#tslices=[0]
#xcent=25.
#pcolormin,pcolormax=1.e-8,1.e-6
#tslices=[270]
#xcent=28.
#tslices=[340,625]
#xcent= length/2.
#pcolormin,pcolormax=5.e-6,1.
#box_width=30.
#slice_loc=[25.,28.,length/2., length/2.]
#kmax=30.
#ampmin,ampmax=-8.,1.
#file_base="del1o30_pspec_"
#nlat=256

#rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/cascade_wfluc/output/"
#length=16.
#slice_loc=[4.,6.,length/2.]
#lperp=64.
#box_width=8.
#tslices=[0]
#xcent=4.
#pcolormin,pcolormax=1.e-12,1.e-8
#tslices=[415]
#xcent=6.
#tslices=[480,700]
#xcent=length/2.
#kmax=32.
#ampmin,ampmax=-12.,2.
#pcolormin,pcolormax = 1.e-3,1.
#file_base="sg_cascade_pspec_"
#nlat=

# Variables Determining the Plotting Output
lmargin=0.2
rmargin=0.95

figheight=720
figwidth=720
topheight=360

varname="rhospec"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

DefineScalarExpression("rhopow", "(coord(mesh)[0])^2*rho*"+str(lperp)+"^2")

AddPlot("Pseudocolor","rhopow")
p=PseudocolorAttributes()
p.min=pcolormin
p.minFlag=1
p.max=pcolormax
p.maxFlag=1
p.scaling=1  # set to log scaling for color
p.invertColorTable=1
p.colorTableName="RdYlBu"
SetPlotOptions(p)

# Set legend properties (ugly, try to make nicer)
objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.HorizontalBottom
l.numberFormat="%# -4.2g"
l.managePosition=0
l.position=(0.1,0.95)
l.xScale=3
l.yScale=1.
l.fontBold=1
l.fontHeight=0.09
l.fontFamily=2

# Now Set the Annotations
a=GetAnnotationAttributes()
a.axes2D.xAxis.title.visible=0
a.axes2D.xAxis.title.userTitle=1
a.axes2D.xAxis.title.title="k/m"
a.axes2D.xAxis.title.font.scale=4
a.axes2D.xAxis.label.visible=0
a.axes2D.xAxis.label.font.scale=3.5

a.axes2D.yAxis.title.userTitle=1
a.axes2D.yAxis.title.title="mx" #_collision"
a.axes2D.yAxis.title.font.scale=4.5
a.axes2D.yAxis.label.font.scale=4.5

a.axes2D.xAxis.title.font.font=2
a.axes2D.yAxis.title.font.font=2
a.axes2D.xAxis.label.font.font=2
a.axes2D.yAxis.label.font.font=2

a.axes2D.tickLocation=0

a.userInfoFlag=0
a.databaseInfoFlag=0
SetAnnotationAttributes(a)

# Set up the viewport properly
v=View2DAttributes()
v.fullFrameActivationMode=0
v.windowCoords=(0, kmax, length/2.-box_width, length/2.+box_width)
v.viewportCoords = (lmargin,rmargin, 0., 0.8)
SetView2D(v)

DrawPlots()

# Now Draw the LineOut
Lineout((0.,xcent),(kmax,xcent))
SetActiveWindow(2)
vc=ViewCurveAttributes()
vc.domainCoords=(0.,kmax)
vc.rangeCoords=(ampmin,ampmax)
vc.rangeScale=vc.LOG
SetViewCurve(vc)

v.viewportCoords = (lmargin,rmargin, 0., 1.)
SetView2D(v)

a.axes2D.xAxis.label.visible=1
a.axes2D.xAxis.title.visible=1
a.axes2D.yAxis.title.title="k^2P(k)"
SetAnnotationAttributes(a)

ca=CurveAttributes()
ca.showLegend=0
ca.curveColorSource=ca.Custom
ca.curveColor = (0,255,0,255)
ca.lineWidth=1
ca.showLabels=0
SetPlotOptions(ca)
SetViewCurve(vc)

# Options to Set, Lock Query to Time, In Window 2, or whatever is created, set curve plot options

# Now set up the multiwindow save
sw=GetSaveWindowAttributes()
sw.format=sw.PNG   # this is PNG
sw.width=figwidth
sw.height=figheight
sw.advancedMultiWindowSave=1
sw.fileName=file_base
SetSaveWindowAttributes(sw)
# Now I can set up the multiwindow save
sw.subWindowAtts.win1.position = (0, figheight-topheight)
sw.subWindowAtts.win1.size = (figwidth, topheight)
sw.subWindowAtts.win2.position = (0, 0)
sw.subWindowAtts.win2.size = (figwidth, figheight-topheight)
sw.screenCapture=0
sw.resConstraint=sw.NoConstraint
SetSaveWindowAttributes(sw)

if paper_plots:
    for tcur in tslices:
        SetActiveWindow(1)
        SetTimeSliderState(tcur)
        SetActiveWindow(2)
        SetTimeSliderState(tcur)
        SetViewCurve(vc)
        DrawPlots()
        SaveWindow()

if movie_plots:
    for i in range(TimeSliderGetNStates()):
        SetActiveWindow(1)
        SetTimeSliderState(i)
        SetActiveWindow(2)
        SetTimeSliderState(i)
        SetViewCurve(vc)

        DrawPlots()
        SaveWindow()
