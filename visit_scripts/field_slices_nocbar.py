PaperPlots=True
MoviePlots=False

#run_selection='sg_v1'
#run_selection='sg_vsqrt2'
#run_selection='shape'
#run_selection='v0.05'
run_selection='del0.1'

def MakeRGBColorTable(name,ct):
    ccpl = ColorControlPointList()
    for pt in ct:
        p = ColorControlPoint()
        p.colors = (pt[0]*255, pt[1]*255, pt[2]*255, 255)
        p.position = pt[3]
        ccpl.AddControlPoints(p)
    AddColorTable(name,ccpl)

cpoints = [ ( 1., 1., 0., 1./12.),
            ( 1. ,0., 0., 1./6.),
            ( 1. ,1., 1., 0.5),
            ( 0., 0., 1., 5./6.),
            ( 0., 1., 1., 11./12.) ]
MakeRGBColorTable("two_well", cpoints)

cpoints_r = [ ( 0., 1., 1., 1./12.),
            ( 0. ,0., 1., 1./6.),
            ( 1. ,1., 1., 0.5),
            ( 1., 0., 0., 5./6.),
            ( 1., 1., 0., 11./12.) ]
MakeRGBColorTable("two_well_r", cpoints_r)

nperiod = 4
space = 1./nperiod
mypi = 3.14159
cpoint_sg = []
for i in range(nperiod):
    left=i*space
    cpoint_sg.append( (0.,0.,1.,left) )
    cpoint_sg.append( (1.,1.,1.,left+0.25*space) )
    cpoint_sg.append( (1.,0.,0.,left+0.5*space) )
    cpoint_sg.append( (1.,1.,1.,left+0.75*space) )
    cpoint_sg.append( (0.,0.,1.,left+space) )
MakeRGBColorTable("SG_zag", cpoint_sg)          

if run_selection=='sg_v1':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/v1/output/"
    tslices=[0,135,195,400]
    length=128
    width=64
    xcent=width/2.
    mycolortable="RdBu"
    minval,maxval=-3,3
    base_file="sg_breather_v1_field_"

elif run_selection=='sg_vsqrt2':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/vsqrt2/output/"
    tslices=[0,702,798,1000]
    length=128.
    width=64.
    xcent=width/2.
    mycolortable="RdBu"
    minval,maxval=-1.5,1.5
    base_file="sg_breather_vsqrt2_field_"

elif run_selection=='shape':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/1wall_wspec/absorb/dx0.25/w2_L32x128_xdotfluc/output/"
    tslices=[0,60,85,400]
    length=128
    width=32
    xcent=width/2.
    mycolortable="difference"
    minval,maxval=-1.,1.
    base_file="shapemode_field_"

elif run_selection=='sg_cascade':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/sine_gordon/dx0.25/cascade_wfluc/output/"
    tslices=[0,415,480,700]
    length=64.
    width=16.
    xcent=width/2.
    mycolortable="SG_zag"
    minval,maxval=-12.*mypi,4.*mypi

#v=0.05 symmetric double well, fix color table
elif run_selection=='v0.05':
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/new_amplitude/absorb/dx0.125/v0.05_L32x128/output/"
    tslices=[340,372,425,1000]
    length=128
    width=32
    xcent=width/2.
    mycolortable="two_well_r"
    minval,maxval=-1.5,1.5
    base_file="v0.05_field_"

elif run_selection=='del0.1':
#del1/30 well
    rootdir="/mnt/scratch-lustre/jbraden/domain_walls/periodic/2walls_wspec/del1o30/absorb/dx0.125/v0_r8_L64x128/output_a5/"
    tslices=[340,625] #[0] #[270] #[340,625]
    length=128
    width=64
    xcent=width/2.   #25. #28.  #width/2.
    mycolortable="two_well_r"
    minval,maxval=-1.5,1.5
    base_file="del1o30_field_"

else:
    print 'Error, invalid choice of plots'

figheight=1024
figwidth=1024

varname="phi1"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
a.axes3D.bboxLocation=(0,length,0,width,-20,length)
a.axes3D.setBBoxLocation=1

a.backgroundMode=1
a.gradientBackgroundStyle=0
a.gradientColor1 = (192,192,192,255)
a.gradientColor2 = (255,255,255,255)

SetAnnotationAttributes(a)

v=GetView3D()
v.viewNormal = (0.7,-0.5,0.2)
v.viewUp=(0,0,1)
v.focus=(length/2.,width/2.,length/2.-15.)
v.imageZoom=1.05
SetView3D(v)

AddPlot("Pseudocolor",varname)
pa=PseudocolorAttributes()
pa.colorTableName=mycolortable
pa.minFlag=1
pa.maxFlag=1
pa.min=minval
pa.max=maxval
pa.smoothingLevel=2
SetPlotOptions(pa)

pa.legendFlag=0
SetPlotOptions(pa)
#objs=GetAnnotationObjectNames()
#l=GetAnnotationObject(objs[0])
#l.drawTitle=0
#l.drawMinMax=0
#l.orientation=l.VerticalRight
#l.numberFormat="%# -5.2g"
#l.managePosition=1
#l.position=(0.04,0.98)
#l.xScale=0.5
#l.yScale=3
#l.fontBold=1
#l.fontHeight=0.12
#l.fontFamily=2

AddOperator("Slice")
sa=SliceAttributes()
sa.originType=sa.Intercept
sa.originIntercept=length/2.
sa.axisType=sa.YAxis
sa.project2d=1
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta=TransformAttributes()
ta.doScale=1
ta.scaleZ=5
SetOperatorOptions(ta)

AddPlot("Pseudocolor",varname)
pa.legendFlag=0
SetPlotOptions(pa)

AddOperator("Slice")
sa.originIntercept=xcent
sa.axisType=sa.ZAxis
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta.doRotate=0
ta.doScale=1
ta.doTranslate=0
ta.scaleZ=2
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=1
ta.rotateAxis=(1,0,0)
ta.rotateAmount=90
ta.doScale=0
ta.doTranslate=0
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=0
ta.doTranslate=1
ta.translateY=width
SetOperatorOptions(ta)

#ts=CreateAnnotationObject("TimeSlider")
#ts.position=(0.2,0.05)
#ts.height=0.1
#ts.width=0.5

DrawPlots()

v=GetView3D()
v.focus=(length/2.,width/2.,length/2.-15)
SetView3D(v)

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=0
SetSaveWindowAttributes(sw)

#for tcur in range(500):
#    SetTimeSliderState(tcur)
#    SaveWindow()

SetTimeSliderState(200)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        SetView3D(v)
        SaveWindow()

if MoviePlots:
    for tcur in range(TimeSliderGetNStates()):
        SetTimeSliderState(tcur)
        SaveWindow()
