PaperPlots=False

rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/bubble_and_wall/bulk_l1e-4_newic/output/"
tslices=[250,450,600]
mycolortable="difference"
length=13.*2.**0.5*10.
width=13.*2.**0.5*10.
minval=-1.2
maxval=1.2

figheight=720
figwidth=720

varname="phi1"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
a.axes3D.bboxLocation=(0,length,0,width,-20,length)
a.axes3D.setBBoxLocation=1
SetAnnotationAttributes(a)

v=GetView3D()
v.viewNormal = (0.7,-0.5,0.2)
v.viewUp=(0,0,1)
v.focus=(length/2.,width/2.,length/2.)
v.imageZoom=0.9
SetView3D(v)

AddPlot("Pseudocolor",varname)
pa=PseudocolorAttributes()
pa.colorTableName=mycolortable
pa.minFlag=1
pa.maxFlag=1
pa.min=minval
pa.max=maxval
pa.smoothingLevel=2
SetPlotOptions(pa)

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.05,0.95)
l.xScale=0.5
l.yScale=3
l.fontBold=1
l.fontHeight=0.05

AddOperator("Slice")
sa=SliceAttributes()
sa.originType=sa.Intercept
sa.originIntercept=length/2.
sa.axisType=sa.YAxis
sa.project2d=1
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta=TransformAttributes()
ta.doScale=1
ta.scaleZ=5
SetOperatorOptions(ta)

AddPlot("Pseudocolor",varname)
pa.legendFlag=0
SetPlotOptions(pa)

AddOperator("Slice")
sa.originIntercept=width/2.+20.
sa.axisType=sa.ZAxis
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta.doRotate=0
ta.doScale=1
ta.doTranslate=0
ta.scaleZ=5
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=1
ta.rotateAxis=(1,0,0)
ta.rotateAmount=90
ta.doScale=0
ta.doTranslate=0
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=0
ta.doTranslate=1
ta.translateY=width
SetOperatorOptions(ta)

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.2,0.05)

DrawPlots()

v=GetView3D()
v.focus=(length/2.,width/2.,length/2.-15.)
SetView3D(v)

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName="field_slices_"
sw.resConstraint=sw.NoConstraint
sw.screenCapture=0
SetSaveWindowAttributes(sw)

#for tcur in range(500):
#    SetTimeSliderState(tcur)
#    SaveWindow()

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        SaveWindow()
