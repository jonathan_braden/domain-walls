import numpy as np
import matplotlib.pyplot as plt

falsevac='blue'
newvac='red'

ymax=6*np.pi
yvals=np.linspace(0.,ymax,501)
wall1=np.sin(yvals)
wall2=-np.sin(yvals)

dist=1.5
plt.fill_betweenx(yvals,wall1+dist,wall2-dist,where=wall1+2*dist>wall2, facecolor=falsevac,hatch='/',alpha=0.5)
plt.xlim(-3.5,3.5)
plt.ylim(0.,ymax)
plt.tick_params(which='both',bottom='off',top='off',left='off',right='off',labelbottom='off',labelleft='off')
plt.savefig('wall_cartoon_1.pdf')
plt.show()

dist=1.
plt.fill_betweenx(yvals,wall1+dist,wall2-dist,where=wall1+2*dist>wall2, facecolor=falsevac,hatch='/',alpha=0.5)
plt.xlim(-3.5,3.5)
plt.ylim(0.,ymax)
plt.tick_params(which='both',bottom='off',top='off',left='off',right='off',labelbottom='off',labelleft='off')
plt.savefig('wall_cartoon_2.pdf')
plt.show()

dist=0.25
plt.fill_betweenx(yvals,wall1+dist,wall2-dist,where=wall1+2*dist>wall2, facecolor=falsevac,hatch='/',alpha=0.5)
plt.fill_betweenx(yvals,wall1+dist,wall2-dist,where=wall1+2*dist<wall2, facecolor=newvac,hatch='+',alpha=0.5)
plt.xlim(-3.5,3.5)
plt.ylim(0.,ymax)
plt.tick_params(which='both',bottom='off',top='off',left='off',right='off',labelbottom='off',labelleft='off')
plt.savefig('wall_cartoon_3.pdf')
plt.show()
